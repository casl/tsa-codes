import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime
import time

ts = time.time()

df = pd.read_csv("tsa_on_aes_timing_0.txt", header=None)

values = list(df[0])

print("The key byte at the attacked location is " + str(hex(np.argmax(values))))
plt.figure()
plt.xlabel("Page Number")
plt.ylabel("Clock Cycle")

plt.plot(values)
plt.scatter([np.argmax(values)],[values[np.argmax(values)]], color='red')

plt.text(np.argmax(values),values[np.argmax(values)], str(np.argmax(values)) + ": " + str(hex(np.argmax(values))))

time_stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
plt.savefig("plots/Figure_demo" + time_stamp + ".png", dpi=400, bbox_inches='tight')

