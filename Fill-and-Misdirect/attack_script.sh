#!/bin/sh

echo "Starting make.."
make clean
make
echo "Make done!"


echo "Running the attack on $1 byte $2 times."

byte=$1
times=$2

for i in $(seq 1 $times); do
	echo "-------------------------------------------------------------"
	echo "Attack attempt $i"
	./attack $byte
	echo "Analyzing.."
	python3 plotter.py
	echo "Actual key:"
	cat key
	echo "--------------------------------------------------------------"
	sleep 1
done
