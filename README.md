# README #


This repository contains the code for Timed Speculative Attack (TSA) strategies, that do not depend on the state changes in the cache memory. The strategies, namely Fill-and-Forward and Fill-and-Misdirect, present in their respective folders use the timing differences occuring due to store-to-load forwarding. Fill-and-Forward presents a stealthy covert channel, while Fill-and-Misdirect demonstrates key recovery on T-Table based OpenSSL AES. Each of the folders contain README files with specific steps to run the code. 

The full paper can be found here: https://dl.acm.org/doi/abs/10.1145/3489517.3530493

## Requirements

    - GCC
	- G++
    - Python pacakges: numpy, pandas, matplotlib, datetime, time, json

## Citation

```
@inproceedings{DBLP:conf/dac/ChakrabortySBRM22,
  author    = {Anirban Chakraborty and
               Nikhilesh Singh and
               Sarani Bhattacharya and
               Chester Rebeiro and
               Debdeep Mukhopadhyay},
  editor    = {Rob Oshana},
  title     = {Timed speculative attacks exploiting store-to-load forwarding bypassing
               cache-based countermeasures},
  booktitle = {{DAC} '22: 59th {ACM/IEEE} Design Automation Conference, San Francisco,
               California, USA, July 10 - 14, 2022},
  pages     = {553--558},
  publisher = {{ACM}},
  year      = {2022},
  url       = {https://doi.org/10.1145/3489517.3530493},
  doi       = {10.1145/3489517.3530493}
}
```
