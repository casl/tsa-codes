## Description

This directory contains codes for the Fill-and-Forward Covert channel exploiting Store-to-Load Forwarding. Read the full paper here: https://dl.acm.org/doi/abs/10.1145/3489517.3530493

## Directory Structure:

```
Fill-and-Misdirect
+-- stol.c
	|__ This is test code to check the execution tiiming differences between a forwarded and a non-forwarded load on your system.
+-- frequency_dist_plotter.py
	|__ Python Code to plot the results from stol.c.
+-- stol_covert.c
	|__ A 1-bit covert channel exploiting Store-to-Load forwarding.
+-- stol_leakage_test.sh
    |-- Shell script to quickly try out the test step.
+-- /plots
    |__ Folder to store resulting plots.
+-- Makefile
	|__ Yes, Makefile!
+-- README
```	

## Leakage Test: Quick Tryout

1. Run the shell script provided with the repository.

```
./stol_leakage_test.sh
```

The shell script runs the tests, builds two timing distributions; one with loads using store-to-load forwarding and the other without forwarding, and plots them. The result can be found in the plots directory. Farther apart the two distributions, higher is the usability of the timing channel on the system.

Following are the steps invovled in detail.


## Leakage Test Steps

While the shell script may work fine for most cases, we can also dig in to the specific source codes for more direct control. Following are the steps to follow.

1. Open terminal, with Fill-and-Forward as the working directory and run the following commands.

```
make clean
make
```
This will clean up the previous make and compile both the test code and the Covert channel code.


2. Run the test binary.

```
./stol_test 0 <output_file_0>
./stol_test 1 <output_file_1>
```

The command line arguments:

    - <0 or 1>: 0 dumps the execution timing using store-to-load forwarding while 1 dumps the execution timing without forwarding.
    - <output_file>: File to dump the timing values. Running it for the cases, we would have two output files after this step.

One thread in the stol_test binary keeps running on, we can either kill it after a while (depends on number of samples) or run with a time out as follows.  


```
timeout 10s ./stol_test 0 <output_file_0>
timeout 10s ./stol_test 1 <output_file_1>
```


3. Now we can run the plotting code.

```
python3 frequency_dist_plotter <output_file_0>, <output_file_1>, <output_image_file>
```


The generated plot can be found in the plots folder. 

## Covert Channel Steps

1. Open terminal, with Fill-and-Forward as the working directory and run the following commands.

```
make clean
make
```
This will clean up the previous make and compile both the test code and the Covert channel code.

2. Run the channel.

```
./stol_covert <bit_to_send>
```

Expected ouput:

```
Sent:0
Received:0
<execution_timing_value>

or

Sent:1
Received:1
<execution_timing_value>
```

The covert channel can be fine-tuned using the parameters mentioned in stol_covert.c.

## Points to Note

1. The number of stores and loads can be altered in stol.c and stol_covert.c for experimentation.
2. The threshold value in stol_covert.c needs to be updated as per the execution environment. 
1. Tested on the following CPUs (so far):


	- Intel Core i5-4285U
	- Intel Core i7-7700
	- Intel Core i5-8500
	- Intel Core i7-3770
	- Intel Core i9-11900


