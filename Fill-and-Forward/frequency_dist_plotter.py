import json
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import time
import sys



if(len(sys.argv) < 4):
    print("command line arguments order: <data_file_without_forwarding>, <data_file_with_forwarding>, <output_image_file>")
    exit()
else:
    # File with execution timing without forwading
    file_wo_forwarding = sys.argv[1]
    # File with execution timing with Store-to-Load forwarding
    file_with_forwarding = sys.argv[2]
    # Image output
    image_name = sys.argv[3] 

ts = time.time()


x = []
y = []

x2 = []
y2 = []



data_file_100_pd = pd.read_csv(file_wo_forwarding, header=None, sep='\s+')
data_file_1_pd = pd.read_csv(file_with_forwarding, header=None, sep='\s+')

data_file_100 = data_file_100_pd[0].tolist()
data_file_1 = data_file_1_pd[0].tolist()

dict_100 = {}
dict_1 = {}

# The maximum and minimum value to consider for plotting
# threshold = 5000
upper = 5000
lower = 500

for i in range(upper):
    if (i < upper and i > lower):
        dict_1[i] = 0
        dict_100[i] = 0

for i in data_file_100:
    if (i < upper and i > lower ):
        dict_100[i] += 1
    
for i in data_file_1:
    if (i < upper and i > lower ):
        dict_1[i] += 1
    
# Without forwarding
for k in dict_100:
    x.append(int(k))
    y.append(int(dict_100[k]))

# With Forwarding
for k in dict_1:
    x2.append(int(k))
    y2.append(int(dict_1[k]))


plt.title("50 load instructions, parent-child setup")
plt.bar(x,y,width=2, color=(0.8, 0.4, 0.2, 0.6), label= "Without forwarding")
plt.bar(x2,y2,width=2,color=(0.2, 0.4, 0.8, 0.6), label= "With forwarding")
#plt.bar(x,y,width=10, color=(0.8, 0.4, 0.2, 0.6), label= "Always misses")
plt.xlabel("Execution timing for load instruction(s)")
plt.ylabel("Frequency")
#plt.xlim(300)
plt.legend()

time_stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
plt.savefig("plots/Figure_StoL_test" + image_name + time_stamp + ".png", dpi=400)
# plt.show()


'''
n_num = [a*b for a, b in zip(x2,y2)]
mean = sum(n_num)/sum(y)
print(str(mean))
n = len(n_num) 
n_num.sort() 

if n % 2 == 0:
    median1 = n_num[n//2]
    median2 = n_num[n//2 - 1]
    median = (median1 + median2)/2
else:
    median = n_num[n//2]
    print("Median is: " + str(median)) 

mean =  sum(n_num)/n
print(str(mean))
'''
