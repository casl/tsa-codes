#!/bin/sh

make clean
make

echo "Starting..."
echo "Measuring execution timing with forwarding"

timeout 10s ./stol_test 0 with_f.txt
echo "Measuring execution timing without forwarding"
sleep 2

timeout 10s ./stol_test 1 wo_f.txt

echo "Plotting the results"

python3 ./frequency_dist_plotter.py wo_f.txt with_f.txt a



