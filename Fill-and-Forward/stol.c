#define _GNU_SOURCE
#include <assert.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <time.h>
#include <asm/unistd.h>
#include <fcntl.h>
#include <inttypes.h>
#include <linux/kernel-page-flags.h>

#include <sched.h>
#include <stdbool.h>
#include <signal.h>
#include <emmintrin.h>

// Number of samples to be taken
#define SAMPLES 10000

// In microseconds
// The sender runs continuosuly, the receiver sleeps for this much time
// to allow the sender to execute.
// The exact value would depend on the average time taken by sender and hence,
// the average time for a load on the system.
#define SLEEP_TIME 10

typedef struct {
    __m128i vec_val;
    int int_val;
    char data[64 - (sizeof(__m128i) + sizeof(int))];
} cache_line;

__attribute__ ((aligned(64)))
cache_line large_buffer[1024];

__attribute__ ((aligned(64)))
cache_line small_buffer[1024];



size_t mem_size = 32*1024;

static unsigned int timestamp(void)	{
	unsigned int bottom, top;
	asm volatile("xorl %%eax, %%eax\n cpuid \n" ::: "%eax", "%ebx", "%ecx", "%edx"); 
	asm volatile("rdtsc\n" : "=a" (bottom), "=d" (top) );	 
	asm volatile("xorl %%eax, %%eax\n cpuid \n" ::: "%eax", "%ebx", "%ecx", "%edx");
	return bottom;
}

void maccess(void* p)
{
	asm volatile ("movq (%0), %%rax\n"
		:
		: "c" (p)
		: "rax");
}


// Non-temporal stores
// This is to avoid any role of the cache
void force_nt_store(cache_line *a) {
    __m128i zeros = {0, 0}; 
    __asm volatile("movntdq %0, (%1)\n\t"
                   "movntdq %0, 16(%1)\n\t"
                   "movntdq %0, 32(%1)\n\t"
                   "movntdq %0, 48(%1)"
                   :
                   : "x" (zeros), "r" (&a->vec_val)
                   : "memory");
}

void mfence() {
   __asm volatile("mfence" ::: "memory");
}





int main(int argc, char *argv[])	{
    int pid;
	
	// Number of stores performed in between the store and load
	int eviction_iter = -1;
	int eviction_mode = -1;

	FILE *data_file;


	if(argc==3){
		
		eviction_mode = atoi(argv[1]);

		if(eviction_mode != 0 && eviction_mode != 1){
		     printf("ERROR: Incorrect choice of mode. 0 for forwarding, 1 for no forwarding.");
			 printf("\n ./stol_test <0 or 1> <filename>\n");
			 return 0;
		}

		data_file = fopen(argv[2], "w");
		
		// No stores performed, ensuring forwarding
		if(eviction_mode == 0)
			eviction_iter = 0;
		// Multiple stores performed, ensuring no forwarding
		else
			eviction_iter = 100;
	}
	else if(argc == 2){
		printf("ERROR: Please provide the choice of mode and file to dump data.\n");
		printf("\n ./stol_test <0 or 1> <filename>\n");
		return 0;
	}
	else if(argc > 3){
		printf("ERROR: Too many arguments.\n");
		printf("\n ./stol_test <0 or 1> <filename>\n");
		return 0;
	}
	else{
		printf("ERROR: Please provide the file to dump data and mode to execute, 0 for forwarding, 1 for no farwarding.");
		printf("\n ./stol_test <0 or 1> <filename>\n");
	    return 0;
	}


   // printf("1"); 
    // Attempting to fork
    if ((pid = fork()) < 0){
        perror("fork");
        exit(1);
    }
    
    // Parent process
    if (pid != 0){

	//printf("1");
	// Set affinity mask
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);
	if(sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1){
	    perror("sched_setaffinity");
            assert(false);
        }	    
   
       // printf("%d\n", pid);
       	    // Outer loop: number of experiments
	    //for (int i=0; i < 100000; i++)   {
    	    while(1){    
    	    // Non temporal stores
		// The parent can fill the entire buffer or just have 
		// no entries; depends on this loop
            	for (int j=0; j<eviction_iter; j++)
                	force_nt_store(&large_buffer[( ( j) * 9 )]);
        	mfence();
	    }
    }

    // Child process
    else{
 	// Set affinity
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(1, &mask);
	if(sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1){
	    perror("sched_setaffinity");
            assert(false);
        }	    
  	// printf("In child process"); 
	// Timestamping variables
	uint64_t start = 0, end = 0;	
	
	for (long i=0; i< SAMPLES; i++ ){
	    // Assuming the child process executes first
	    // Do a store; this has to followed by parent process
		// These are 64 stores, comment out the ones that are needed for the experiment
	    force_nt_store(&small_buffer[0]);
	    force_nt_store(&small_buffer[9]);
        force_nt_store(&small_buffer[18]);
	    force_nt_store(&small_buffer[27]);
	    force_nt_store(&small_buffer[36]);
	    force_nt_store(&small_buffer[45]);
	    force_nt_store(&small_buffer[54]);
	    force_nt_store(&small_buffer[63]);
	    force_nt_store(&small_buffer[72]);
	    force_nt_store(&small_buffer[81]);
	    force_nt_store(&small_buffer[90]);
	    force_nt_store(&small_buffer[99]);
        force_nt_store(&small_buffer[108]);
	    force_nt_store(&small_buffer[117]);
	    force_nt_store(&small_buffer[126]);
	    force_nt_store(&small_buffer[135]);
	    force_nt_store(&small_buffer[144]);
	    force_nt_store(&small_buffer[153]);
	    force_nt_store(&small_buffer[162]);
	    force_nt_store(&small_buffer[171]);
	    force_nt_store(&small_buffer[180]);
	    force_nt_store(&small_buffer[189]);
	    force_nt_store(&small_buffer[198]);
	    force_nt_store(&small_buffer[207]);
	    force_nt_store(&small_buffer[216]);
	    force_nt_store(&small_buffer[225]);
	    force_nt_store(&small_buffer[234]);
	    force_nt_store(&small_buffer[243]);
	    force_nt_store(&small_buffer[252]);
	    force_nt_store(&small_buffer[261]);
	    force_nt_store(&small_buffer[270]);
        force_nt_store(&small_buffer[279]);
        force_nt_store(&small_buffer[288]);
        force_nt_store(&small_buffer[297]);
        force_nt_store(&small_buffer[306]);
        force_nt_store(&small_buffer[315]);
        force_nt_store(&small_buffer[324]);
        force_nt_store(&small_buffer[333]);
        force_nt_store(&small_buffer[342]);
        force_nt_store(&small_buffer[351]);
        force_nt_store(&small_buffer[360]);
		force_nt_store(&small_buffer[369]);
	    force_nt_store(&small_buffer[378]);
		force_nt_store(&small_buffer[387]);
		force_nt_store(&small_buffer[396]);
        force_nt_store(&small_buffer[405]);
        force_nt_store(&small_buffer[414]);
        force_nt_store(&small_buffer[423]);
		force_nt_store(&small_buffer[432]);
		force_nt_store(&small_buffer[441]);
		force_nt_store(&small_buffer[450]);
		// force_nt_store(&small_buffer[459]);
		// force_nt_store(&small_buffer[468]);
        // force_nt_store(&small_buffer[477]);
	    // force_nt_store(&small_buffer[486]);
        // force_nt_store(&small_buffer[495]);
        // force_nt_store(&small_buffer[504]);
        // force_nt_store(&small_buffer[513]);
        // force_nt_store(&small_buffer[522]);
        // force_nt_store(&small_buffer[531]);
        // force_nt_store(&small_buffer[540]);


	    
	    
	    mfence();
	
	    // Adding a sleep to give the parent a chance to run 
	    usleep(SLEEP_TIME);

	    // Do a load to the same address as previous store
	    // Difference would occur based on whether parent removed 
	    // previous store from the store buffer
	    start = timestamp();

	    
		// These are 60 loads, comment out the the ones that are not needed for the experiment
	    maccess(&small_buffer[0]);
	    maccess(&small_buffer[9]);
	    maccess(&small_buffer[18]);
        maccess(&small_buffer[27]);
        maccess(&small_buffer[36]);
        maccess(&small_buffer[45]);
        maccess(&small_buffer[54]);
        maccess(&small_buffer[63]);
        maccess(&small_buffer[72]);
        maccess(&small_buffer[81]);
        maccess(&small_buffer[90]);
	    maccess(&small_buffer[99]);
	    maccess(&small_buffer[108]);
        maccess(&small_buffer[117]);
        maccess(&small_buffer[126]);
        maccess(&small_buffer[135]);
        maccess(&small_buffer[144]);
        maccess(&small_buffer[153]);
        maccess(&small_buffer[162]);
        maccess(&small_buffer[171]);
	    maccess(&small_buffer[180]);
        maccess(&small_buffer[189]);
        maccess(&small_buffer[198]);
        maccess(&small_buffer[207]);
        maccess(&small_buffer[216]);
        maccess(&small_buffer[225]);
        maccess(&small_buffer[234]);
        maccess(&small_buffer[243]);
	    maccess(&small_buffer[252]);
        maccess(&small_buffer[261]);
        maccess(&small_buffer[270]);
        maccess(&small_buffer[279]);
        maccess(&small_buffer[288]);
        maccess(&small_buffer[297]);
        maccess(&small_buffer[306]);
        maccess(&small_buffer[315]);
	    maccess(&small_buffer[324]);
        maccess(&small_buffer[333]);
        maccess(&small_buffer[342]);
        maccess(&small_buffer[351]);
        maccess(&small_buffer[360]);
        maccess(&small_buffer[369]);
        maccess(&small_buffer[378]);
        maccess(&small_buffer[387]);
	    maccess(&small_buffer[396]);
        maccess(&small_buffer[405]);
        maccess(&small_buffer[414]);
        maccess(&small_buffer[423]);
        maccess(&small_buffer[432]);
        maccess(&small_buffer[441]);
        maccess(&small_buffer[450]);
        // maccess(&small_buffer[459]);
        // maccess(&small_buffer[468]);
        // maccess(&small_buffer[477]);
        // maccess(&small_buffer[486]);
        // maccess(&small_buffer[495]);
        // maccess(&small_buffer[504]);
        // maccess(&small_buffer[513]);
        // maccess(&small_buffer[522]);
        // maccess(&small_buffer[531]);
	    // maccess(&small_buffer[540]);
        
        
        mfence();
	    end = timestamp();
	    
	
	    fprintf(data_file, "%lu\n", end-start);
	    }
	}
	// printf("Execution complete, Hit CTRL+C\n");
        return 0;
    }
    

    


