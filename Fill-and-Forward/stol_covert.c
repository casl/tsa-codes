#define _GNU_SOURCE
#include <assert.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <inttypes.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <time.h>
#include <asm/unistd.h>
#include <fcntl.h>
#include <inttypes.h>
#include <linux/kernel-page-flags.h>

#include <sched.h>
#include <stdbool.h>
#include <signal.h>
#include <emmintrin.h>

// This is to be cofigured as per the system
// And the number of stores and loads used
#define THRESHOLD 2050

// In microseconds
// The sender runs continuosuly, the receiver sleeps for this much time
// to allow the sender to execute.
// The exact value would depend on the average time taken by sender and hence,
// the average time for a load on the system.
#define SLEEP_TIME 10

typedef struct {
    __m128i vec_val;
    int int_val;
    char data[64 - (sizeof(__m128i) + sizeof(int))];
} cache_line;

__attribute__ ((aligned(64)))
cache_line large_buffer[1024];

__attribute__ ((aligned(64)))
cache_line small_buffer[1024];



size_t mem_size = 32*1024;

static unsigned int timestamp(void)	{
	unsigned int bottom, top;
	asm volatile("xorl %%eax, %%eax\n cpuid \n" ::: "%eax", "%ebx", "%ecx", "%edx"); 
	asm volatile("rdtsc\n" : "=a" (bottom), "=d" (top) );	 
	asm volatile("xorl %%eax, %%eax\n cpuid \n" ::: "%eax", "%ebx", "%ecx", "%edx");
	return bottom;
}

void maccess(void* p)
{
	asm volatile ("movq (%0), %%rax\n"
		:
		: "c" (p)
		: "rax");
}

// Non-temporal stores
void force_nt_store(cache_line *a) {
    __m128i zeros = {0, 0}; 
    __asm volatile("movntdq %0, (%1)\n\t"
                   "movntdq %0, 16(%1)\n\t"
                   "movntdq %0, 32(%1)\n\t"
                   "movntdq %0, 48(%1)"
                   :
                   : "x" (zeros), "r" (&a->vec_val)
                   : "memory");
}


void mfence() {
   __asm volatile("mfence" ::: "memory");
}





int main(int argc, char *argv[])	{
    int pid;
    int tosend = 0;

    if(argc==2){
        tosend = atoi(argv[1]);

	if(tosend != 0 && tosend!= 1){
		printf("Hey, that's not a bit!\n");
		return 0;
		}
    }
    else if(argc > 2){
    	printf("This version supports one bit at a time!\n");
    	return 0;
    }
    else{
    	printf("Please provide the bit to communicate accross\n");
    	return 0;
    }
   
    // Attempting to fork
    if ((pid = fork()) < 0){
        perror("fork");
        exit(1);
    }
    
    // Parent process
    if (pid != 0){

	// Set affinity mask
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(0, &mask);
	if(sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1){
	    perror("sched_setaffinity");
            assert(false);
        }	    
 
   
   printf("Sent:%d\n", tosend);
   
   if (tosend){
           for (;;){    
    	    	for (int j=0; j<100; j++)
                	force_nt_store(&large_buffer[(( j) * 9)]);
        	mfence();
	    }
	  }
	  
	else{
        for (; ; ){    
    	// Non temporal stores
		// The parent can fill the entire buffer or just have 
		// one entry; depends on this loop
        	for (int j=0; j<0; j++)
            	force_nt_store(&large_buffer[(( j) * 9)]);
        	mfence();
	    }
	  }
    
    }

    // Child process
    else{
	 	// Set affinity
		cpu_set_t mask;
		CPU_ZERO(&mask);
		CPU_SET(1, &mask);
		// Setting both the processes on the same logical core
		if(sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1){
		    perror("sched_setaffinity");
	            assert(false);
	        }	    
	  	//printf("In child process"); 
		// Timestamping variables
	    // This can be handled in a better way as well.
		int arr_timings[10000];
	
		uint64_t start = 0, end = 0;	
	

		for (long i=0; i< 10000; i++ ){
		    // Assuming the child process executes first
		    // Do a store; this has to followed by parent process
		    // The number of store-loads can be configured
		    force_nt_store(&small_buffer[0]);
		    force_nt_store(&small_buffer[9]);
	        force_nt_store(&small_buffer[18]);
		    force_nt_store(&small_buffer[27]);
		    force_nt_store(&small_buffer[36]);
		    force_nt_store(&small_buffer[45]);
		    force_nt_store(&small_buffer[54]);
		    force_nt_store(&small_buffer[63]);
		    force_nt_store(&small_buffer[72]);
		    force_nt_store(&small_buffer[81]);
		    force_nt_store(&small_buffer[90]);
		    force_nt_store(&small_buffer[99]);
	        force_nt_store(&small_buffer[108]);
		    force_nt_store(&small_buffer[117]);
		    force_nt_store(&small_buffer[126]);
		    force_nt_store(&small_buffer[135]);
		    force_nt_store(&small_buffer[144]);
		    force_nt_store(&small_buffer[153]);
		    force_nt_store(&small_buffer[162]);
		    force_nt_store(&small_buffer[171]);
		    force_nt_store(&small_buffer[180]);
	        force_nt_store(&small_buffer[189]);
	        force_nt_store(&small_buffer[198]);
	        force_nt_store(&small_buffer[207]);
	        force_nt_store(&small_buffer[216]);
	        force_nt_store(&small_buffer[225]);
	        force_nt_store(&small_buffer[234]);
	        force_nt_store(&small_buffer[243]);
	        force_nt_store(&small_buffer[252]);
	        force_nt_store(&small_buffer[261]);
	        force_nt_store(&small_buffer[270]);
	        force_nt_store(&small_buffer[279]);
	        force_nt_store(&small_buffer[288]);
	        force_nt_store(&small_buffer[297]);
	        force_nt_store(&small_buffer[306]);
	        force_nt_store(&small_buffer[315]);
	        force_nt_store(&small_buffer[324]);
	        force_nt_store(&small_buffer[333]);
	        force_nt_store(&small_buffer[342]);
	        force_nt_store(&small_buffer[351]);
		    force_nt_store(&small_buffer[360]);
	        force_nt_store(&small_buffer[369]);
	        force_nt_store(&small_buffer[378]);
	        force_nt_store(&small_buffer[387]);
	        force_nt_store(&small_buffer[396]);
	        force_nt_store(&small_buffer[405]);
	        force_nt_store(&small_buffer[414]);
	        force_nt_store(&small_buffer[423]);
	        force_nt_store(&small_buffer[432]);
	        force_nt_store(&small_buffer[441]);
	        force_nt_store(&small_buffer[450]);
	     //    force_nt_store(&small_buffer[459]);
	     //    force_nt_store(&small_buffer[468]);
	     //    force_nt_store(&small_buffer[477]);
	     //    force_nt_store(&small_buffer[486]);
	     //    force_nt_store(&small_buffer[495]);
		 //    force_nt_store(&small_buffer[504]);
	     //    force_nt_store(&small_buffer[513]);
	     //    force_nt_store(&small_buffer[522]);
	     //    force_nt_store(&small_buffer[531]);
	     //    force_nt_store(&small_buffer[540]);



		    
		    
		    mfence();
		
		    // Adding a sleep to give the parent a chance to run 
		    usleep(SLEEP_TIME);

		    // Do a load to the same address as previous store
		    // Difference would occur based on whether parent removed 
		    // previous store from the store buffer
		    start = timestamp();
		    
		    maccess(&small_buffer[0]);
		    maccess(&small_buffer[9]);
		    maccess(&small_buffer[18]);
	        maccess(&small_buffer[27]);
	        maccess(&small_buffer[36]);
	        maccess(&small_buffer[45]);
	        maccess(&small_buffer[54]);
	        maccess(&small_buffer[63]);
	        maccess(&small_buffer[72]);
	        maccess(&small_buffer[81]);
	        maccess(&small_buffer[90]);
		    maccess(&small_buffer[99]);
		    maccess(&small_buffer[108]);
	        maccess(&small_buffer[117]);
	        maccess(&small_buffer[126]);
	        maccess(&small_buffer[135]);
	        maccess(&small_buffer[144]);
	        maccess(&small_buffer[153]);
	        maccess(&small_buffer[162]);
	        maccess(&small_buffer[171]);
		    maccess(&small_buffer[180]);
	        maccess(&small_buffer[189]);
	        maccess(&small_buffer[198]);
	        maccess(&small_buffer[207]);
	        maccess(&small_buffer[216]);
	        maccess(&small_buffer[225]);
	        maccess(&small_buffer[234]);
	        maccess(&small_buffer[243]);
	        maccess(&small_buffer[252]);
	        maccess(&small_buffer[261]);
	        maccess(&small_buffer[270]);
	        maccess(&small_buffer[279]);
	        maccess(&small_buffer[288]);
	        maccess(&small_buffer[297]);
	        maccess(&small_buffer[306]);
	        maccess(&small_buffer[315]);
	        maccess(&small_buffer[324]);
	        maccess(&small_buffer[333]);
	        maccess(&small_buffer[342]);
	        maccess(&small_buffer[351]);
		    maccess(&small_buffer[360]);
	        maccess(&small_buffer[369]);
	        maccess(&small_buffer[378]);
	        maccess(&small_buffer[387]);
	        maccess(&small_buffer[396]);
	        maccess(&small_buffer[405]);
	        maccess(&small_buffer[414]);
	        maccess(&small_buffer[423]);
	        maccess(&small_buffer[432]);
	        maccess(&small_buffer[441]);
	        maccess(&small_buffer[450]);
	     //    maccess(&small_buffer[459]);
	     //    maccess(&small_buffer[468]);
	     //    maccess(&small_buffer[477]);
	     //    maccess(&small_buffer[486]);
	     //    maccess(&small_buffer[495]);
	     //    maccess(&small_buffer[504]);
	     //    maccess(&small_buffer[513]);
	     //    maccess(&small_buffer[522]);
	     //    maccess(&small_buffer[531]);
		 //    maccess(&small_buffer[540]);


	        mfence();
		    end = timestamp();
		    
		    arr_timings[i] = end-start;
		    //printf("%lu\n", end-start);
		}
	
		int sum=0;
	    for(int i=0; i<10000; i++){
	        sum += arr_timings[i];
	    }

	    if (sum/10000 > THRESHOLD){
	        printf("Received: 1\n");
		    printf("%d", sum/10000);
	    }
	    else{
	        printf("Received: 0\n");
		    printf("%d", sum/10000);
		}
	 // }
	}
    
    return 0;
}